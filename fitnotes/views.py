from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib import auth
from django.http import HttpResponseRedirect

from fitnotes.fitlogs.models import UserProfile,FitLog
from fitnotes.forms import RegistrationForm
from django.contrib.auth.models import User


def home(request):
	if request.user.is_authenticated():
		return render_to_response('home.html',
								{'user':True,'username': request.user})
	else:
	   return render_to_response('home.html')

def Fitlogs_View(request):	
	if request.user.is_authenticated():
		userlogs = FitLog.objects.filter(Created_by=request.user)
		return render_to_response('fitlogs/userlogs.html',
								{'user':True,'username':request.user,'logs':userlogs})
	else:
		return render_to_response('fitlogs/userlogs.html',)
	
	
def fitforum(request):
	if request.user.is_authenticated():
		return render_to_response('fitforums.html',
								{'user':True,'username':request.user})
	else:
	   return render_to_response('fitforums.html')

def Register_User(request):
	if request.method =='POST':
		form= RegistrationForm(request.POST)
					
		if form.is_valid():
			cd= form.cleaned_data
			newuser= User.objects.create_user(cd['username'].lower(), cd['email'], cd['password1'])
																																																														 
			new_profile = UserProfile(user=newuser,)
			new_profile.save()
		
			user = auth.authenticate(username=cd['username'].lower(), password=cd['password1'])
			if user is not None and user.is_active:
				auth.login(request, user)
				return HttpResponseRedirect("/")
	else:
		
		form = RegistrationForm()
		
	return render_to_response('registration/signup.html',
							{'form': form},
							context_instance=RequestContext(request))
	