from django.contrib import admin
from fitnotes.fitlogs.models import FitLog,Exercise,UserProfile


class ExcerAdmin(admin.ModelAdmin):
    list_display=('created_by','workout')
    ordering = ('created_by',)
    
class LogsAdmin(admin.ModelAdmin):
    list_display=('log_Date','type','weight','Created_by')
    list_filter=('log_Date',)
    date_hierarchy = 'log_Date'
    ordering = ('-log_Date',)
    fields = ('log_Date', 'type', 'workouts','weight')
    filter_horizontal = ('workouts',)


class UserAdmin(admin.ModelAdmin):
    
    list_display=('user',)
    fields = ('user','journalEntries',)
    search_fields =('user',)
    ordering = ('user',)
    filter_horizontal = ('journalEntries',)



admin.site.register(Exercise,ExcerAdmin)
admin.site.register(FitLog,LogsAdmin)
admin.site.register(UserProfile,UserAdmin)