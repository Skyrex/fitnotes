from django.db import models
from django.contrib.auth.models import User
 
   
# Create your models here.
class Exercise(models.Model):
    workout=models.CharField(max_length=80)
    duration=models.CharField(max_length=20)
    reps=models.CharField(max_length=20)
    resistance=models.CharField(max_length=40)
    created_by = models.CharField(max_length=60)
    def __unicode__(self):
        return self.workout

class FitLog(models.Model):
    Created_by = models.CharField(max_length=60)
    log_Date=models.DateField()
    type = models.CharField(max_length = 30,)
    workouts = models.ManyToManyField(Exercise,blank = True)
    weight = models.CharField(max_length=60,)
    def __unicode__(self):
		return u'%s' % self.log_Date
        
    class Meta:
		ordering =['-log_Date']

class UserProfile(models.Model):
	user = models.OneToOneField(User)
	journalEntries = models.ManyToManyField(FitLog, blank = True)

    
    
	