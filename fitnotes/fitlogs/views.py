
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.forms.formsets import formset_factory

from fitnotes.forms import ExerciseForm,FitlogForm
from fitnotes.fitlogs.models import FitLog,Exercise
from django.contrib.auth.decorators import login_required



def Process_data(request):
    data = FitLog.objects.filter(Created_by=request.user,log_Date__range=["2012-08-01", "2013-02-28"])
    total = num = 0
    datelist = []
    for d in data:
        datelist.append(d.log_Date)
        total +=  int(d.weight)
        num += 1
    aver = total / num
    
    return render_to_response('fitlogs/progress.html',
                              {'user':True, 'username':request.user,'aver':aver,'dates':datelist})
    



@login_required(login_url='/login/')
def FitLog_Handler(request):    
    exFormSet = formset_factory(ExerciseForm,extra=1)
    
    if request.method =='POST':
        fitform = FitlogForm(request.POST)    
        formset = exFormSet(request.POST)
        
        if 'del_workout' in request.POST:
            formset,fitform = formset_handler(request,exFormSet,-1)
        
        elif 'add_workout' in request.POST:
            formset,fitform = formset_handler(request,exFormSet,1)
        
        elif 'submit' in request.POST:                        
            if fitform.is_valid() and formset.is_valid():
                submit_form(request,fitform,formset)
                return HttpResponseRedirect("/fitlogs/")
    else:
        fitform = FitlogForm()
        formset = exFormSet()
        
    return render_to_response('fitlogs/logform.html',
                            {'user':True,'username':request.user,'form':fitform,'formset':formset},
                            context_instance=RequestContext(request))

def submit_form(request,fitform,formset):
    log = fitform.save(commit=False)
    log.Created_by = request.user
    log.save()
    for values in formset.cleaned_data:
        exer = Exercise(workout=values['workout'],reps = values['reps'], 
                     duration = values['duration'], resistance=values['resistance'], created_by = request.user)
        exer.save()
        log.workouts.add(exer)
    
def formset_handler(request, exFormSet,num):
    cp = request.POST.copy()
    cp['form-TOTAL_FORMS'] = int(cp['form-TOTAL_FORMS']) + num
    formset = exFormSet(cp,prefix='form')
    fitform = FitlogForm(cp) 
    return formset,fitform