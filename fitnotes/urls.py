from django.conf.urls import *
from django.contrib import admin

from fitnotes.views import home,Fitlogs_View,fitforum,Register_User
from fitnotes.fitlogs.views import FitLog_Handler, Process_data

from django.contrib.auth.views import login, logout



admin.autodiscover()

urlpatterns = patterns('',

	(r'^admin/', include(admin.site.urls)),
	(r'^$',home),
	(r'^fitlogs/$',Fitlogs_View),
	(r'^fitlogs/logit$',FitLog_Handler),
	(r'^fitlogs/progress$',Process_data),
	
	(r'^fitforums/$',fitforum),
	
	
	(r'^login/$',  login, {'extra_context': {'next': '/'}}),
	(r'^logout/$', logout,{'next_page': '/'}),
	
	(r'^signup/$', Register_User),
	
)
              